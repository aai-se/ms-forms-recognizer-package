package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.NullPointerException;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetDocumentInfoAsJsonCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetDocumentInfoAsJsonCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetDocumentInfoAsJson command = new GetDocumentInfoAsJson();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("LookupURL") && parameters.get("LookupURL") != null && parameters.get("LookupURL").get() != null) {
      convertedParameters.put("LookupURL", parameters.get("LookupURL").get());
      if(!(convertedParameters.get("LookupURL") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LookupURL", "String", parameters.get("LookupURL").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LookupURL") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LookupURL"));
    }

    if(parameters.containsKey("AutoRetry") && parameters.get("AutoRetry") != null && parameters.get("AutoRetry").get() != null) {
      convertedParameters.put("AutoRetry", parameters.get("AutoRetry").get());
      if(!(convertedParameters.get("AutoRetry") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","AutoRetry", "Boolean", parameters.get("AutoRetry").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("AutoRetry") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","AutoRetry"));
    }
    if(convertedParameters.get("AutoRetry") != null && (Boolean)convertedParameters.get("AutoRetry")) {
      if(parameters.containsKey("MaxTimeout") && parameters.get("MaxTimeout") != null && parameters.get("MaxTimeout").get() != null) {
        convertedParameters.put("MaxTimeout", parameters.get("MaxTimeout").get());
        if(!(convertedParameters.get("MaxTimeout") instanceof Number)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","MaxTimeout", "Number", parameters.get("MaxTimeout").get().getClass().getSimpleName()));
        }
      }
      if(convertedParameters.get("MaxTimeout") == null) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","MaxTimeout"));
      }
      if(convertedParameters.containsKey("MaxTimeout")) {
        try {
          if(convertedParameters.get("MaxTimeout") != null && !((double)convertedParameters.get("MaxTimeout") > 60)) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.GreaterThan","MaxTimeout", "60"));
          }
        }
        catch(ClassCastException e) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","MaxTimeout", "Number", convertedParameters.get("MaxTimeout").getClass().getSimpleName()));
        }
        catch(NullPointerException e) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","MaxTimeout"));
        }

      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("LookupURL"),(Boolean)convertedParameters.get("AutoRetry"),(Number)convertedParameters.get("MaxTimeout")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
