package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class UploadDocumentCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(UploadDocumentCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    UploadDocument command = new UploadDocument();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("TheFile") && parameters.get("TheFile") != null && parameters.get("TheFile").get() != null) {
      convertedParameters.put("TheFile", parameters.get("TheFile").get());
      if(!(convertedParameters.get("TheFile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","TheFile", "String", parameters.get("TheFile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("TheFile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","TheFile"));
    }

    if(parameters.containsKey("Mode") && parameters.get("Mode") != null && parameters.get("Mode").get() != null) {
      convertedParameters.put("Mode", parameters.get("Mode").get());
      if(!(convertedParameters.get("Mode") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","Mode", "String", parameters.get("Mode").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("Mode") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","Mode"));
    }
    if(convertedParameters.get("Mode") != null) {
      switch((String)convertedParameters.get("Mode")) {
        case "MODEL" : {
          if(parameters.containsKey("ModelID") && parameters.get("ModelID") != null && parameters.get("ModelID").get() != null) {
            convertedParameters.put("ModelID", parameters.get("ModelID").get());
            if(!(convertedParameters.get("ModelID") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ModelID", "String", parameters.get("ModelID").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("ModelID") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ModelID"));
          }


        } break;
        case "TABLE" : {

        } break;
        case "ID" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","Mode"));
      }
    }

    if(parameters.containsKey("APIVersion") && parameters.get("APIVersion") != null && parameters.get("APIVersion").get() != null) {
      convertedParameters.put("APIVersion", parameters.get("APIVersion").get());
      if(!(convertedParameters.get("APIVersion") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","APIVersion", "String", parameters.get("APIVersion").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("APIVersion") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","APIVersion"));
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("TheFile"),(String)convertedParameters.get("Mode"),(String)convertedParameters.get("ModelID"),(String)convertedParameters.get("APIVersion")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
