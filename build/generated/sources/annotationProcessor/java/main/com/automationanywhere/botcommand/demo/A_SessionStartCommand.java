package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class A_SessionStartCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(A_SessionStartCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    A_SessionStart command = new A_SessionStart();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("BringYourOwnAccount") && parameters.get("BringYourOwnAccount") != null && parameters.get("BringYourOwnAccount").get() != null) {
      convertedParameters.put("BringYourOwnAccount", parameters.get("BringYourOwnAccount").get());
      if(!(convertedParameters.get("BringYourOwnAccount") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BringYourOwnAccount", "Boolean", parameters.get("BringYourOwnAccount").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("BringYourOwnAccount") != null && (Boolean)convertedParameters.get("BringYourOwnAccount")) {
      if(parameters.containsKey("BYOBaseURI") && parameters.get("BYOBaseURI") != null && parameters.get("BYOBaseURI").get() != null) {
        convertedParameters.put("BYOBaseURI", parameters.get("BYOBaseURI").get());
        if(!(convertedParameters.get("BYOBaseURI") instanceof String)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BYOBaseURI", "String", parameters.get("BYOBaseURI").get().getClass().getSimpleName()));
        }
      }

      if(parameters.containsKey("BYOSubscriptionKey") && parameters.get("BYOSubscriptionKey") != null && parameters.get("BYOSubscriptionKey").get() != null) {
        convertedParameters.put("BYOSubscriptionKey", parameters.get("BYOSubscriptionKey").get());
        if(!(convertedParameters.get("BYOSubscriptionKey") instanceof SecureString)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BYOSubscriptionKey", "SecureString", parameters.get("BYOSubscriptionKey").get().getClass().getSimpleName()));
        }
      }
      if(convertedParameters.get("BYOSubscriptionKey") == null) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","BYOSubscriptionKey"));
      }

    }

    command.setSessions(sessionMap);
    command.setGlobalSessionContext(globalSessionContext);
    try {
      command.start((String)convertedParameters.get("sessionName"),(Boolean)convertedParameters.get("BringYourOwnAccount"),(String)convertedParameters.get("BYOBaseURI"),(SecureString)convertedParameters.get("BYOSubscriptionKey"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","start"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
