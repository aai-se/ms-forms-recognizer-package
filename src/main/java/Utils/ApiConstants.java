package Utils;

public class ApiConstants {
        private ApiConstants() {}

        public static final String APP_BASE_URI = "https://iqbot.cognitiveservices.azure.com/formrecognizer";
        public static final String API_V200_PREVIEW = "v2.0-preview";
        public static final String API_V212_PREVIEW = "v2.1-preview.2";
        public static final String API_V213_PREVIEW = "v2.1-preview.3";
        public static final String API_V200 = "v2.0";
        public static final String IQBOT_API_KEY = "f856e71f033147cd82c109115ef6d51a";
        //public static final String IQBOT_API_KEY = "65c6482e-21f8-4ba7-8205-bcee8d1460f1s";

}
