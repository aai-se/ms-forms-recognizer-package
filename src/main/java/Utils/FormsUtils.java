package Utils;

import com.automationanywhere.core.security.SecureString;

public class FormsUtils {
    public static SecureString StringToSecureString(String myString){

        char[] SecStringArr = new char[myString.length()];
        for (int i = 0; i < myString.length(); i++) { SecStringArr[i] = myString.charAt(i); }
        SecureString secString = new SecureString(SecStringArr);
        return secString;
    }
}
