package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RestResponses {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    private static final Logger logger = LogManager.getLogger(RestResponses.class);
    public RestResponses(){ }

    public  ArrayList<DictionaryValue> GetDocumentInfoAsList(String JSONContent) throws IOException, ParseException, FailedFetchException {

        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();

        String JsonResponse = JSONContent;

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);

        String oStatus = (String)jobj.get("status");

        if(oStatus.equalsIgnoreCase("succeeded")){

            JSONObject allResults = (JSONObject)jobj.get("analyzeResult");
            JSONArray allObjects = (JSONArray)allResults.get("documentResults");

            JSONObject RootEntry = (JSONObject)allObjects.get(0);

            JSONObject AllFields = (JSONObject)RootEntry.get("fields");

            // for each key
            AllFields.keySet().forEach(keyName -> {
                //System.out.println("Processing Key:"+keyName);
                try{
                    HashMap<String, Value> ObjectMap = new HashMap<String,Value>();
                    DictionaryValue ObjectDict = new DictionaryValue();

                    JSONObject KeyInfo = (JSONObject) AllFields.get(keyName);
                    String KeyValString = (String) KeyInfo.get("valueString");
                    String KeyText = (String) KeyInfo.get("text");
                    Double KeyConfidence = (Double) KeyInfo.get("confidence");
                    Long KeyPage = (Long) KeyInfo.get("page");
                    String KeyType = (String) KeyInfo.get("type");
                    KeyConfidence = KeyConfidence*100;
                    JSONArray KeyBoundingBox = (JSONArray) KeyInfo.get("boundingBox");

                    ObjectMap.put("text",new StringValue(KeyText));
                    ObjectMap.put("keyname",new StringValue(keyName.toString()));
                    ObjectMap.put("type",new StringValue(KeyType));
                    ObjectMap.put("page",new StringValue(KeyPage.toString()));
                    ObjectMap.put("score",new StringValue(KeyConfidence.toString()));
                    //.out.println("DEBUG SIZE:"+KeyBoundingBox.size());

                    Double[] AllCoordinates = new Double[8];

                    for(int a = 0;a<8;a++){
                        Double Coord = (Double) KeyBoundingBox.get(a);
                        AllCoordinates[a] = Coord;
                        //ObjectMap.put("X"+a,new StringValue(Coord.toString()));
                        //System.out.println("DEBUG Coord:"+Coord);
                    }
                    Double X0 = AllCoordinates[0];
                    Double Y0 = AllCoordinates[1];
                    Double Width = AllCoordinates[2] - X0;
                    Double Height = AllCoordinates[7] - Y0;

                    ObjectMap.put("x",new StringValue(X0.toString()));
                    ObjectMap.put("y",new StringValue(Y0.toString()));
                    ObjectMap.put("width",new StringValue(Width.toString()));
                    ObjectMap.put("height",new StringValue(Height.toString()));

                    ObjectDict.set(ObjectMap);
                    AllValues.add(ObjectDict);
                    //System.out.println("key: "+ keyName + " value: " + KeyText+" - "+KeyConfidence);

                }catch(Exception e){
                    //System.out.println("ERR:"+e.getMessage());
                    logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+e.getMessage()) ;
                    throw new BotCommandException(MESSAGES.getString("APIError", e.getMessage()));
                }
            });
            return AllValues;
        }else if(oStatus.equalsIgnoreCase("failed")){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+JsonResponse) ;
            throw new BotCommandException(MESSAGES.getString("APIError", JsonResponse));
        }
        else{
            return null;
        }

    }

    public Table GetDocumentInfoAsTable(String JsonResponse) throws IOException, ParseException, FailedFetchException {

        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        String oStatus = (String)jobj.get("status");
        //System.out.println("RES:"+JsonResponse);

        Table table = new Table();

        List<Row> AllRows = new ArrayList<Row>();

        // Setting up Headers
        List<Schema> ListOfHeaders = new ArrayList<Schema>();
        ListOfHeaders.add(new Schema("type"));
        ListOfHeaders.add(new Schema("keyname"));
        ListOfHeaders.add(new Schema("content"));
        ListOfHeaders.add(new Schema("text"));
        ListOfHeaders.add(new Schema("score"));
        ListOfHeaders.add(new Schema("page"));
        ListOfHeaders.add(new Schema("x"));
        ListOfHeaders.add(new Schema("y"));
        ListOfHeaders.add(new Schema("width"));
        ListOfHeaders.add(new Schema("height"));
        table.setSchema(ListOfHeaders);

        if(oStatus.equalsIgnoreCase("succeeded")){

            JSONObject allResults = (JSONObject)jobj.get("analyzeResult");
            //JSONArray allObjects = (JSONArray)allResults.get("readResults");
            JSONArray allObjects = (JSONArray)allResults.get("documentResults");

            JSONObject RootEntry = (JSONObject)allObjects.get(0);

            JSONObject AllFields = (JSONObject)RootEntry.get("fields");
            //System.out.println("DEBUG All Fields:"+AllFields.toString());
            // for each key
            AllFields.keySet().forEach(keyName ->
            {

                try{
                    //System.out.println("DEBUG Key Name:"+keyName);
                    if(AllFields.get(keyName) != null){
                        JSONObject KeyInfo = (JSONObject) AllFields.get(keyName);

                        String KeyValType = (String) KeyInfo.get("type");
                        String KeyValue = "";
                        if(KeyValType.equalsIgnoreCase("date")){ KeyValue = (String) KeyInfo.get("valueDate");}
                        if(KeyValType.equalsIgnoreCase("string")){ KeyValue = (String) KeyInfo.get("valueString");}

                        String KeyText = "";
                        if(KeyInfo.get("text")!=null){
                            KeyText = (String) KeyInfo.get("text");
                        }

                        String KeyConfidenceStr = "";
                        if(KeyInfo.get("confidence") != null && KeyInfo.get("confidence") instanceof Double){
                            Double KeyConfidence = (Double) KeyInfo.get("confidence");
                            KeyConfidence = KeyConfidence*100;
                            KeyConfidenceStr = KeyConfidence.toString();
                        }

                        if(KeyInfo.get("confidence") != null && KeyInfo.get("confidence") instanceof Long){
                            Long KeyConfidence = (Long) KeyInfo.get("confidence");
                            KeyConfidence = KeyConfidence*100;
                            KeyConfidenceStr = KeyConfidence.toString();
                        }

                        //System.out.println("DEBUG Conf Score:"+KeyConfidenceStr);

                        String PageNumStr = "";
                        if(KeyInfo.get("page")!= null){
                            if(KeyInfo.get("page") instanceof Long){PageNumStr=((Long) KeyInfo.get("page")).toString();}
                            if(KeyInfo.get("page") instanceof Double){PageNumStr=((Double) KeyInfo.get("page")).toString();}
                        }else{
                            PageNumStr = "0";
                        }

                        JSONArray KeyBoundingBox = (JSONArray) KeyInfo.get("boundingBox");

                        String X0Text = "";
                        String Y0Text = "";
                        String WidthText = "";
                        String HeightText = "";

                        Double[] AllCoordinates = new Double[8];

                        if(KeyBoundingBox != null){
                            for(int a = 0;a<8;a++){
                                //System.out.println("Debug: Processing element:"+a);
                                if(KeyBoundingBox.get(a) instanceof Long){
                                    Double Coord = (Double) ((Long) KeyBoundingBox.get(a)).doubleValue();
                                    AllCoordinates[a] = Coord;
                                }else{
                                    Double Coord = (Double) KeyBoundingBox.get(a);
                                    AllCoordinates[a] = Coord;
                                }
                            }
                        }else{
                            for(int a = 0;a<8;a++){
                                AllCoordinates[a] = 0d;
                            }
                        }

                        Double X0 = AllCoordinates[0];
                        X0Text = X0.toString();
                        Double Y0 = AllCoordinates[1];
                        Y0Text = Y0.toString();
                        Double Width = AllCoordinates[2] - X0;
                        WidthText = Width.toString();
                        Double Height = AllCoordinates[7] - Y0;
                        HeightText = Height.toString();

                        Row aRow = new Row();
                        List<Value> ListOfValues = new ArrayList<Value>();
                        ListOfValues.add(new StringValue(KeyValType));
                        ListOfValues.add(new StringValue(keyName.toString()));
                        ListOfValues.add(new StringValue(KeyValue));
                        ListOfValues.add(new StringValue(KeyText));
                        ListOfValues.add(new StringValue(KeyConfidenceStr));
                        ListOfValues.add(new StringValue(PageNumStr));
                        ListOfValues.add(new StringValue(X0Text));
                        ListOfValues.add(new StringValue(Y0Text));
                        ListOfValues.add(new StringValue(WidthText));
                        ListOfValues.add(new StringValue(HeightText));

                        aRow.setValues(ListOfValues);
                        AllRows.add(aRow);
                    }

                    //System.out.println(aRow.getValues().toString());
                    //System.out.println("key: "+ keyName + " value: " + KeyText+" - "+KeyConfidence);

                }catch(Exception e){
                    logger.info("[MS Form Recognizer] - Error: : "+e.getMessage()) ;
                    e.printStackTrace();
                    throw new BotCommandException(MESSAGES.getString("APIError", e.getMessage()));
                }
            });
            table.setRows(AllRows);
            return table;
        }else if(oStatus.equalsIgnoreCase("failed")){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+JsonResponse) ;
            throw new BotCommandException(MESSAGES.getString("APIError", JsonResponse));
        }
        else{
            return null;
        }

    }

    public ArrayList<TableValue> GetKYCInfoAsTable(String JsonResponse) throws IOException, ParseException, FailedFetchException {

        ArrayList<TableValue> AllTables = new ArrayList<TableValue>();

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        String oStatus = (String)jobj.get("status");

        if(oStatus.equalsIgnoreCase("succeeded")){

            JSONObject allResults = (JSONObject)jobj.get("analyzeResult");
            JSONArray allDocuments = (JSONArray)allResults.get("documentResults");

            for(int i = 0;i<allDocuments.size();i++){

                JSONObject DocData = (JSONObject) allDocuments.get(i);

                Table table = new Table();
                List<Row> AllRows = new ArrayList<Row>();

                // Setting up Headers
                List<Schema> ListOfHeaders = new ArrayList<Schema>();
                ListOfHeaders.add(new Schema("type"));
                ListOfHeaders.add(new Schema("keyname"));
                ListOfHeaders.add(new Schema("text"));
                ListOfHeaders.add(new Schema("score"));
                //ListOfHeaders.add(new Schema("page"));
                //ListOfHeaders.add(new Schema("x"));
                //ListOfHeaders.add(new Schema("y"));
                //ListOfHeaders.add(new Schema("width"));
                //ListOfHeaders.add(new Schema("height"));
                table.setSchema(ListOfHeaders);

                JSONObject AllFields = (JSONObject)DocData.get("fields");

                // for each key
                AllFields.keySet().forEach(keyName ->
                {

                        JSONObject KeyInfo = (JSONObject) AllFields.get(keyName);

                        String KeyText = (String) KeyInfo.get("text");
                        String KeyConfidence = GetNumberAsString(KeyInfo,"confidence",true); //(Double) KeyInfo.get();
                        //String KeyPage = GetNumberAsString(KeyInfo,"page");
                        Long KeyPage = (Long) KeyInfo.get("page");
                        String KeyType = (String) KeyInfo.get("type");

                        JSONArray KeyBoundingBox = (JSONArray) KeyInfo.get("boundingBox");

                        Row aRow = new Row();
                        List<Value> ListOfValues = new ArrayList<Value>();
                        ListOfValues.add(new StringValue(KeyType));
                        ListOfValues.add(new StringValue(keyName.toString()));
                        ListOfValues.add(new StringValue(KeyText));
                        ListOfValues.add(new StringValue(KeyConfidence));
                        //ListOfValues.add(new StringValue(Long.toString(KeyPage)));

                        Long[] AllCoordinates = new Long[8];

                        //for(int a = 0;a<8;a++){
                       //     Long Coord = (Long) KeyBoundingBox.get(a);
                       //     AllCoordinates[a] = Coord;
                       // }
                       // Long X0 = AllCoordinates[0];
                       // Long Y0 = AllCoordinates[1];
                    //Long Width = AllCoordinates[2] - X0;
                   // Long Height = AllCoordinates[7] - Y0;

                       // ListOfValues.add(new StringValue(X0.toString()));
                        //ListOfValues.add(new StringValue(Y0.toString()));
                       // ListOfValues.add(new StringValue(Width.toString()));
                       // ListOfValues.add(new StringValue(Height.toString()));

                        aRow.setValues(ListOfValues);
                        //System.out.println("DEBUG:"+aRow.toString());
                        AllRows.add(aRow);

                    });
                    table.setRows(AllRows);
                    TableValue aTValue = new TableValue();
                    aTValue.set(table);
                    AllTables.add(aTValue);

            }
            return AllTables;
        }else if(oStatus.equalsIgnoreCase("failed")){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+JsonResponse) ;
            throw new BotCommandException(MESSAGES.getString("APIError", JsonResponse));
        }
        else{
            return null;
        }

    }

    public ArrayList<TableValue> GetFormInfoAsTable(String JsonResponse) throws IOException, ParseException, FailedFetchException {

        ArrayList<TableValue> AllTables = new ArrayList<TableValue>();

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);

        String oStatus = (String)jobj.get("status");
        //System.out.println("RES:"+JsonResponse);

        if(oStatus.equalsIgnoreCase("succeeded")){

            JSONObject allResults = (JSONObject)jobj.get("analyzeResult");
            JSONArray allPages = (JSONArray)allResults.get("pageResults");

            //System.out.println("DEBUG1");
            for(int i = 0;i<allPages.size();i++){
                //System.out.println("DEBUG2: Page: "+i);
                JSONObject PageData = (JSONObject) allPages.get(i);
                Long pageNumber = (Long) PageData.get("page");
                JSONArray TablesOnPage = (JSONArray) PageData.get("tables");

                for(int TableNum = 0;TableNum<TablesOnPage.size();TableNum++){
                    // System.out.println("DEBUG3: Table: "+TableNum);
                    JSONObject aTableData = (JSONObject) TablesOnPage.get(TableNum);
                    Long NumberOfRows = (Long) aTableData.get("rows");
                    Long NumberOfColumns = (Long) aTableData.get("columns");
                    JSONArray AllCells = (JSONArray) aTableData.get("cells");

                    Table table = new Table();
                    List<Row> AllRows = new ArrayList<Row>();

                    List<Schema> ListOfHeaders = new ArrayList<Schema>();

                    // System.out.println("Creating "+NumberOfColumns+" Headers");

                    for(int k = 0;k<NumberOfColumns;k++){
                        int ColNameInt = k+1;
                        String ColName = Integer.toString(ColNameInt);
                        ListOfHeaders.add(new Schema(ColName));
                    }

                    table.setSchema(ListOfHeaders);

                    for(int g = 0;g<AllCells.size();g+=NumberOfColumns){
                        // System.out.println("DEBUG4 - new Row: "+g);
                        Row aRow = new Row();
                        List<Value> ListOfValues = new ArrayList<Value>();
                        int currentColumnPos = 0;
                        while(currentColumnPos<NumberOfColumns){
                            //System.out.println("Cell Number relative:"+currentColumnPos);
                            //System.out.println("Cell Number absolute:"+g+currentColumnPos);
                            if(g+currentColumnPos<AllCells.size()){
                                JSONObject CellInfo = (JSONObject) AllCells.get(g+currentColumnPos);
                                Long rowIndex = (Long) CellInfo.get("rowIndex");
                                //System.out.println("DEBUG5 - Row IDX: "+rowIndex);
                                Long columnIndex = (Long) CellInfo.get("columnIndex");
                                String CellText = (String) CellInfo.get("text");
                                ListOfValues.add(new StringValue(CellText));
                            }
                            currentColumnPos++;
                        }
                        aRow.setValues(ListOfValues);
                        AllRows.add(aRow);
                    }
                    table.setRows(AllRows);
                    TableValue aTValue = new TableValue();
                    aTValue.set(table);
                    AllTables.add(aTValue);
                }
            }
            return AllTables;
        }else if(oStatus.equalsIgnoreCase("failed")){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+JsonResponse) ;
            throw new BotCommandException(MESSAGES.getString("APIError", JsonResponse));
            //throw new FailedFetchException(JsonResponse);
            // return AllValues;
        }
        else{
            return null;
        }

    }

    private String GetNumberAsString(JSONObject jObj, String objKey){
        return GetNumberAsString(jObj,objKey,false);
    }
    private String GetNumberAsString(JSONObject jObj, String objKey,boolean isConfScore){
        try{
            Double jObjD = (Double) jObj.get(objKey);
            if(isConfScore){jObjD = jObjD*100;}
            return Double.toString(jObjD);
        }catch(ClassCastException c){
            Long jObjL = (Long) jObj.get(objKey);
            if(isConfScore){jObjL = jObjL*100;}
            return Long.toString(jObjL);
        }catch(NullPointerException e){
            Long jObjL = (Long) jObj.get(objKey);
            if(isConfScore){jObjL = jObjL*100;}
            return Long.toString(jObjL);
        }

    }



}