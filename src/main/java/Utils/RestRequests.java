package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.demo.JSONKYCOutputToCSV;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.*;

public class RestRequests {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    private static final Logger logger = LogManager.getLogger(RestRequests.class);
    private String BACKENDURL = "";
    private String SUBSCRIPTIONKEY = "";

    public RestRequests(){
    }

    public RestRequests(String BackendURL, String SubKey){
        this.BACKENDURL = BackendURL;
        this.SUBSCRIPTIONKEY = SubKey;
    }

    public  ArrayList<DictionaryValue> GetDocumentInfoAsList(String JSONContent) throws IOException, ParseException, FailedFetchException {

        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();

        String JsonResponse = JSONContent;

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);

        String oStatus = (String)jobj.get("status");
        //System.out.println("RES:"+JsonResponse);

        if(oStatus.equalsIgnoreCase("succeeded")){

            JSONObject allResults = (JSONObject)jobj.get("analyzeResult");
            JSONArray allObjects = (JSONArray)allResults.get("documentResults");

            JSONObject RootEntry = (JSONObject)allObjects.get(0);

            JSONObject AllFields = (JSONObject)RootEntry.get("fields");

            // for each key
            AllFields.keySet().forEach(keyName ->
            {
                //System.out.println("Processing Key:"+keyName);
                try{
                    HashMap<String, Value> ObjectMap = new HashMap<String,Value>();
                    DictionaryValue ObjectDict = new DictionaryValue();

                    JSONObject KeyInfo = (JSONObject) AllFields.get(keyName);
                    String KeyValString = (String) KeyInfo.get("valueString");
                    String KeyText = (String) KeyInfo.get("text");
                    Double KeyConfidence = (Double) KeyInfo.get("confidence");
                    Long KeyPage = (Long) KeyInfo.get("page");
                    String KeyType = (String) KeyInfo.get("type");
                    KeyConfidence = KeyConfidence*100;
                    JSONArray KeyBoundingBox = (JSONArray) KeyInfo.get("boundingBox");

                    ObjectMap.put("text",new StringValue(KeyText));
                    ObjectMap.put("keyname",new StringValue(keyName.toString()));
                    ObjectMap.put("type",new StringValue(KeyType));
                    ObjectMap.put("page",new StringValue(KeyPage.toString()));
                    ObjectMap.put("score",new StringValue(KeyConfidence.toString()));
                    //.out.println("DEBUG SIZE:"+KeyBoundingBox.size());

                    Double[] AllCoordinates = new Double[8];

                    for(int a = 0;a<8;a++){
                        Double Coord = (Double) KeyBoundingBox.get(a);
                        AllCoordinates[a] = Coord;
                        //ObjectMap.put("X"+a,new StringValue(Coord.toString()));
                        //System.out.println("DEBUG Coord:"+Coord);
                    }
                    Double X0 = AllCoordinates[0];
                    Double Y0 = AllCoordinates[1];
                    Double Width = AllCoordinates[2] - X0;
                    Double Height = AllCoordinates[7] - Y0;

                    ObjectMap.put("x",new StringValue(X0.toString()));
                    ObjectMap.put("y",new StringValue(Y0.toString()));
                    ObjectMap.put("width",new StringValue(Width.toString()));
                    ObjectMap.put("height",new StringValue(Height.toString()));

                    ObjectDict.set(ObjectMap);
                    AllValues.add(ObjectDict);
                    //System.out.println("key: "+ keyName + " value: " + KeyText+" - "+KeyConfidence);

                }catch(Exception e){
                    //System.out.println("ERR:"+e.getMessage());

                }
            });
            return AllValues;
        }else if(oStatus.equalsIgnoreCase("failed")){
            throw new FailedFetchException(JsonResponse);
            // return AllValues;
        }
        else{
            return null;
        }

    }

    public  ArrayList<DictionaryValue> GetDocumentInfo(String InfoURL) throws IOException, ParseException, FailedFetchException {

        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();

        CloseableHttpClient client1 = HttpClients.createDefault();

        HttpGet httpget = new HttpGet(InfoURL);
        httpget.setHeader("Ocp-Apim-Subscription-Key",this.SUBSCRIPTIONKEY);
        CloseableHttpResponse response = client1.execute(httpget);
        String JsonResponse = EntityUtils.toString(response.getEntity());
        return GetDocumentInfoAsList(JsonResponse);
    }

    public  String GetDocumentInfoAsJSON(String InfoURL) throws IOException, ParseException, FailedFetchException {
        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();
        CloseableHttpClient client1 = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(InfoURL);
        httpget.setHeader("Ocp-Apim-Subscription-Key",this.SUBSCRIPTIONKEY);
        CloseableHttpResponse response = client1.execute(httpget);
        String JsonResponse = EntityUtils.toString(response.getEntity());

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        //System.out.println(jobj.toJSONString());
        String oStatus = (String)jobj.get("status");

        Boolean ApiError = false;
        if(jobj.containsKey("error")){
            ApiError = true;
            JSONObject ApiErrorObj = (JSONObject) jobj.get("error");
            String oCode = (String) ApiErrorObj.get("code");
            String oMsg = (String) ApiErrorObj.get("message");
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: ["+oCode+"] - "+oMsg) ;
            //throw new BotCommandException(MESSAGES.getString("APIError", oMsg));
        }

        if(!ApiError){
            if(oStatus.equalsIgnoreCase("succeeded")){
                return JsonResponse;

            }else if(oStatus.equalsIgnoreCase("failed")){
                logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: ["+JsonResponse+"]") ;
                throw new BotCommandException(MESSAGES.getString("APIError", JsonResponse));
                //throw new FailedFetchException(JsonResponse);
                // return AllValues;
            }
            else{
                return null;
            }
        }else{
            return null;
        }
    }

    private String GetMimeType(File file){
        Path path = file.toPath();
        String mimeType ="";
        try {
            mimeType = Files.probeContentType(path);
        } catch (IOException e) {
            logger.info("[MS Form Recognizer] - Internal Error (getMimeType)") ;
            throw new BotCommandException(MESSAGES.getString("APIError", "GetMimeType"));
           // e.printStackTrace();
        }
        return mimeType;
    }

    private static byte[] readContentIntoByteArray(File file) {
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try
        {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        }
        catch (Exception e)
        {
            logger.info("[MS Form Recognizer] - Internal Error (readFileIntoByteArray)") ;
            throw new BotCommandException(MESSAGES.getString("APIError", "readFileIntoByteArray"));
            //e.printStackTrace();
        }
        return bFile;
    }

    public String UploadDocumentToKYCModel(String FilePath, String APIVersion){
        String CompleteURL =  this.BACKENDURL+"/"+APIVersion+"/prebuilt/idDocument/analyze?includeTextDetails=True";
        return UploadDocumentGeneric(FilePath,CompleteURL);
    }

    public String UploadDocumentToMSModel(String FilePath, String ModelID, String APIVersion){
        ///v2.0/custom/models/b5854174-6064-49bf-8258-da65366e24e4/analyze?includeTextDetails=True
        String CompleteURL =  this.BACKENDURL+"/"+APIVersion+"/custom/models/"+ModelID+"/analyze?includeTextDetails=True";
        return UploadDocumentGeneric(FilePath,CompleteURL);
    }

    public String UploadDocument(String FilePath, String APIVersion){
        // https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.1-preview.2/layout/analyze
        String CompleteURL =  this.BACKENDURL+"/"+APIVersion+"/layout/analyze";
        return UploadDocumentGeneric(FilePath,CompleteURL);
    }

    private String UploadDocumentGeneric(String FilePath, String URLEndpoint){
        String CompleteURL =  URLEndpoint;
        try
        {
            File file = new File(FilePath);
            String mimeType =GetMimeType(file);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost uploadFile = new HttpPost(CompleteURL);

            uploadFile.addHeader("Content-Type",mimeType); // "image/jpeg"
            uploadFile.addHeader("Ocp-Apim-Subscription-Key",this.SUBSCRIPTIONKEY);

            uploadFile.setEntity(new FileEntity(new File(FilePath),ContentType.create(mimeType)));

            CloseableHttpResponse response = httpClient.execute(uploadFile);
            System.out.println("URL:"+CompleteURL);

            Header[] RespHeaders = response.getAllHeaders();
            int RetCode = response.getStatusLine().getStatusCode();

            if(RetCode>=400){
                throw new BotCommandException(MESSAGES.getString("UploadError", response.toString()));
            }

            //System.out.println(response.toString());
            //System.out.println("Return Code From Upload:"+RetCode);
            String RespCheckURL = "";
            for(int i =0;i<RespHeaders.length;i++){
                Header h = RespHeaders[i];

                System.out.println("Resp Header:"+h.getName()+"|"+h.getValue());
                if(h.getName().equals("Operation-Location")){
                    RespCheckURL=h.getValue();
                }
            }

            return RespCheckURL;
        }
        catch (Exception e)
        {
            logger.info("[MS Form Recognizer] - Document Upload Error") ;
            throw new BotCommandException(MESSAGES.getString("UploadError", e.getMessage()));
            //System.out.println(e.getMessage());
        }
    }

}

