package com.automationanywhere.botcommand.demo;

import Utils.ApiConstants;
import Utils.BackendServer;
import Utils.FormsUtils;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */


@BotCommand
@CommandPkg(
        label = "Session Start",
        group_label = "Session Management",
        name = "SessionStart",
        description = "Session Start",
        icon = "pkg.svg",
        node_label = "session start {{sessionName}}|"
)

public class A_SessionStart {

    private static final Logger logger = LogManager.getLogger(A_SessionStart.class);

    @Sessions
    private Map<String, Object> sessions;

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext private com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = AttributeType.CHECKBOX) @Pkg(label = "Bring Your Own", description ="" ) Boolean BringYourOwnAccount,
                      @Idx(index = "2.1", type = AttributeType.TEXT) @Pkg(label = "Base URI",description = "Ex: "+ApiConstants.APP_BASE_URI)  String BYOBaseURI,
                      @Idx(index = "2.2", type = CREDENTIAL) @Pkg(label = "Subscription Key",  default_value_type = STRING, default_value = "") @NotEmpty SecureString BYOSubscriptionKey


    ) {

        // Check for existing session
        if (this.sessions.containsKey(sessionName)){ throw new BotCommandException(MESSAGES.getString("sessionInUse",sessionName)) ;}
        if(BringYourOwnAccount==null){BringYourOwnAccount=false;}
        if(BYOBaseURI == null){BYOBaseURI="";}
        if(BYOSubscriptionKey == null){BYOSubscriptionKey= FormsUtils.StringToSecureString("");}

        BackendServer myBackendServ;
        if(BringYourOwnAccount){
            logger.info("MS Forms - Using BYO License.");
            myBackendServ = new BackendServer(BYOBaseURI,BYOSubscriptionKey.getInsecureString());
        }else{
            myBackendServ = new BackendServer(ApiConstants.APP_BASE_URI,ApiConstants.IQBOT_API_KEY);
        }

        this.sessions.put(sessionName, myBackendServ);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
