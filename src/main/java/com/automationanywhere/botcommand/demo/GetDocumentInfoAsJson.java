package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.FailedFetchException;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThan;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 *
 */

@BotCommand
@CommandPkg(
        label="Get Document Info as JSON",
        group_label = "Extraction",
        name="Get Document Info as JSON",
        description="Get Document Info as JSON",
        icon="pkg.svg",
        node_label="Get Document Info as JSON",
        return_type= STRING,
        return_label="JSON String",
        return_required=true
)

public class GetDocumentInfoAsJson {

    private static final Logger logger = LogManager.getLogger(GetDocumentInfoAsJson.class);
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Lookup URL", default_value_type = STRING,  default_value = "") @NotEmpty String LookupURL,
            @Idx(index = "3", type = CHECKBOX) @Pkg(label = "Auto-Retry", default_value_type = BOOLEAN,  default_value = "True") @NotEmpty Boolean AutoRetry,
            @Idx(index = "3.1", type = NUMBER) @Pkg(label = "Timeout (seconds)", default_value_type = DataType.NUMBER,  default_value = "120") @NotEmpty @GreaterThan("60") Number MaxTimeout
    )
    {

        int Timeout = MaxTimeout.intValue() * 1000;

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String BackendURL = serv.getURL();
        String SubKey = serv.getSubToken();

        // Initiate the RestRequests packaged in a separate class
        RestRequests restTools = new RestRequests(BackendURL,SubKey);

        String JSONOutput = "";
        try {
            JSONOutput = restTools.GetDocumentInfoAsJSON(LookupURL);
            int Cycle = 1000; //ms

            if(JSONOutput==null || JSONOutput.equals("")){
                // Retry after 1 second, then 2 second, then 4, then 8, then.. n*2 for a max of 64 seconds
                if(AutoRetry){
                    while((JSONOutput==null||JSONOutput.equals("")) && Cycle< Timeout){
                        //System.out.println("IN");
                        try {
                            Thread.sleep(Cycle);
                            JSONOutput = restTools.GetDocumentInfoAsJSON(LookupURL);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Cycle = Cycle *2;
                    }
                    if(Cycle >= Timeout){
                        logger.info("[MS Form Recognizer] - Error: Timeout Reached while trying to retrieve: "+LookupURL) ;
                        throw new BotCommandException(MESSAGES.getString("timeoutReached", LookupURL));
                    }
                }else{
                    logger.info("[MS Form Recognizer] - Error: Document is not done being processed: "+LookupURL) ;
                    throw new BotCommandException(MESSAGES.getString("tooEarly", LookupURL));
                }
            }
        } catch (IOException e) {
            logger.info("[MS Form Recognizer] - Error: IO: "+e.getMessage()) ;
            //throw new BotCommandException(MESSAGES.getString("IOError", e.getMessage()));
        } catch (ParseException e) {
            logger.info("[MS Form Recognizer] - Error: Parse: "+e.getMessage()) ;
            throw new BotCommandException(MESSAGES.getString("ParseError", e.getMessage()));
            //e.printStackTrace();
        }catch (FailedFetchException e){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+e.getMessage()) ;
            throw new BotCommandException(MESSAGES.getString("failedFetchFile", e.getMessage()));
        }

        return new StringValue(JSONOutput);

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
