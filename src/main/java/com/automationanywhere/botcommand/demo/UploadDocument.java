package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 *
 */

@BotCommand
@CommandPkg(
        label="Upload Document",
        group_label = "Extraction",
        name="Upload Document",
        description="Upload document",
        icon="pkg.svg",
        node_label="Upload Document",
        return_type= STRING, return_label="URL With Results",
        return_required=true
)

public class UploadDocument {

    private static final Logger logger = LogManager.getLogger(UploadDocument.class);
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile,
            @Idx(index = "3", type = AttributeType.SELECT, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(label = "Model Based Extraction", value = "MODEL")),
                    @Idx.Option(index = "3.2", pkg = @Pkg(label = "Layout Based Extraction", value = "TABLE")),
                    @Idx.Option(index = "3.3", pkg = @Pkg(label = "KYC Based Extraction", value = "ID"))
            }) @Pkg(label = "Mode") @NotEmpty String Mode,
            @Idx(index = "3.1.1", type = TEXT) @Pkg(label = "Model ID", default_value_type = STRING,  default_value = "") @NotEmpty String ModelID,
            @Idx(index = "4", type = TEXT) @Pkg(label = "API Version", default_value_type = STRING,  default_value = "v2.0") @NotEmpty String APIVersion

            )
    {

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String BackendURL = serv.getURL();
        String SubKey = serv.getSubToken();

        // Initiate the RestRequests packaged in a separate class
        RestRequests restTools = new RestRequests(BackendURL,SubKey);

        String ResURL = "";
        if(Mode.equalsIgnoreCase("MODEL")){
            ResURL = restTools.UploadDocumentToMSModel(TheFile,ModelID,APIVersion);
        }else if(Mode.equalsIgnoreCase("TABLE")){
            ResURL = restTools.UploadDocument(TheFile,APIVersion);
        }else if(Mode.equalsIgnoreCase("ID")){
            ResURL = restTools.UploadDocumentToKYCModel(TheFile,APIVersion);

        }

        return new StringValue(ResURL);

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
