package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.FailedFetchException;
import Utils.RestRequests;
import Utils.RestResponses;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 *
 */

@BotCommand
@CommandPkg(
        label="Convert Table Info To Table",
        group_label = "Table - Transformers",
        name="JSON Table to Table",
        description="JSON Table to Table",
        icon="pkg.svg",
        node_label="JSON Table to Table",
        return_type= DataType.LIST,
        return_label="List of Tables",
        return_required=true
)

public class JSONTableOutputToCSV {

    private static final Logger logger = LogManager.getLogger(JSONTableOutputToCSV.class);
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<?> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "JSON String", default_value_type = STRING,  default_value = "") @NotEmpty String JSONInput
    )
    {

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        ArrayList<TableValue> AllTables = new ArrayList<TableValue>();
        ListValue RetList = new ListValue();
        // Initiate the RestRequests packaged in a separate class
        RestResponses restTools = new RestResponses();

        Table outputTable = new Table();
        TableValue tv = new TableValue();

            try {//GetFormInfoAsTable
                ArrayList<TableValue> AllExtractedTableValues = restTools.GetFormInfoAsTable(JSONInput);

                RetList.set(AllExtractedTableValues);

            } catch (IOException e) {
                logger.info("[MS Form Recognizer] - Error: IO: "+e.getMessage()) ;
                throw new BotCommandException(MESSAGES.getString("IOError", e.getMessage()));
            } catch (ParseException e) {
                logger.info("[MS Form Recognizer] - Error: Parse: "+e.getMessage()) ;
                throw new BotCommandException(MESSAGES.getString("ParseError", e.getMessage()));
            }catch (FailedFetchException e){
                logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+e.getMessage()) ;
                throw new BotCommandException(MESSAGES.getString("failedFetchFile", e.getMessage()));
            }

        return RetList;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
