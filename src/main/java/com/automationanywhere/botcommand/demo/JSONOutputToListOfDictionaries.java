package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.FailedFetchException;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 *
 */

@BotCommand
@CommandPkg(
        label="Convert Document Info To List",
        group_label = "Model - Transformers",
        name="JSON to List of Dictionaries",
        description="JSON to List of Dictionaries",
        icon="pkg.svg",
        node_label="JSON to List of Dictionaries",
        return_type= DataType.LIST,
        return_sub_type = DataType.ANY,
        return_label="list of dictionaries with 9 keys: 'page','keyname','type','text','score','x','y','width','height'",
        return_required=true
)

public class JSONOutputToListOfDictionaries {

    private static final Logger logger = LogManager.getLogger(JSONOutputToListOfDictionaries.class);
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "JSON String", default_value_type = STRING,  default_value = "") @NotEmpty String JSONInput
    )
    {

        ArrayList<DictionaryValue> AllDetectedObjects = new ArrayList<DictionaryValue>();

        // Initiate the RestRequests packaged in a separate class
        RestRequests restTools = new RestRequests();

        try {
            AllDetectedObjects = restTools.GetDocumentInfoAsList(JSONInput);

        } catch (IOException e) {
            logger.info("[MS Form Recognizer] - Error: IO: "+e.getMessage()) ;
            throw new BotCommandException(MESSAGES.getString("IOError", e.getMessage()));
        } catch (ParseException e) {
            logger.info("[MS Form Recognizer] - Error: Parse: "+e.getMessage()) ;
            throw new BotCommandException(MESSAGES.getString("ParseError", e.getMessage()));
        }catch (FailedFetchException e){
            logger.info("[MS Form Recognizer] - Error: Fetching Status and Info failed: "+e.getMessage()) ;
            throw new BotCommandException(MESSAGES.getString("failedFetchFile", e.getMessage()));
        }

        ListValue AllObjects = new ListValue();
        AllObjects.set(AllDetectedObjects);

        return AllObjects;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
