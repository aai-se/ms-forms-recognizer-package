package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class JSONOutputToListOfDictionariesTest2 {

    JSONOutputToListOfDictionaries command = new JSONOutputToListOfDictionaries();
    GetDocumentInfoAsJson command0 = new GetDocumentInfoAsJson();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

              //  {"https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/b5854174-6064-49bf-8258-da65366e24e4/analyzeresults/6d6d76e9-df87-4672-887f-10ee6390f660",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheURL, String TheRes){
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";

        String TheFile = "C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Desktop\\Deloitte\\Form 1120_SCH L_1-2018-1.pdf";
        String ModelID = "94448316-7d88-4e79-9e66-6d349743ca7d";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);

        command.setSessions(mso);
        command0.setSessions(mso);

        StringValue val = command0.action("Default",TheURL,true,64);
        String JSONOutput = val.toString();
        //System.out.println("Callback URL:"+TheCallBackURL);
        ListValue d = command.action(JSONOutput);

        List<Value> myList = d.get();

        for(Value v: myList){
            Map<String,Value> myMap = (Map<String, Value>) v.get();
            // = dict.get();

            for (Map.Entry<String,Value> entry : myMap.entrySet()) {
                System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            }
        }

    }
}
