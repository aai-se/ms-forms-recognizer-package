package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class UploadDocumentTest {

    UploadDocument command = new UploadDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

             //   {"C:\\iqbot\\Document Samples\\Stock Documents\\Classifier_Training_Folder\\To_Sort\\AK.jpeg","0299bbe0-be05-4143-a3a0-dce59762efc2",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFile, String ModelID, String TheRes){
        String URL = "https://msformrecog.cognitiveservices.azure.com/formrecognizer/v2.0-preview/custom/models/";
        String AuthToken = "cd47bcd0cd50490cbf2dd905c646264e";
        String APIVersion = "v2.0";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        StringValue d = command.action("Default",TheFile,"MODEL",ModelID,APIVersion);

        System.out.println("URL:"+d.toString());

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
