package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

@Test(enabled=true)
public class GetContentTest {

    GetDocumentInfoAsJson command = new GetDocumentInfoAsJson();
    //UploadDocument command0 = new UploadDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

               // {"",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheURL, String TheRes){
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";

        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);



        command.setSessions(mso);
        //command0.setSessions(mso);


        String TheCallbackURL = "https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/b5854174-6064-49bf-8258-da65366e24e4/analyzeresults/8fc2183f-410a-4b00-b2ee-8bfa704396c5";

       // StringValue val = command0.action("Default",TheFile,ModelID);
        //String TheCallBackURL = val.toString();
        System.out.println("Callback URL:"+TheCallbackURL);
        StringValue d = command.action("Default",TheCallbackURL,true,64);

        //System.out.println(d);


    }
}
