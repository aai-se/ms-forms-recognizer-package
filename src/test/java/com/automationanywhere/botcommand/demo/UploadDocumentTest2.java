package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

@Test(enabled=true)
public class UploadDocumentTest2 {

    UploadDocument command = new UploadDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

            //    {"C:\\iqbot\\MSForms\\88852 MCDONALD AUTOMOTIVE CLARE, INC. 0920 (1).pdf","b5854174-6064-49bf-8258-da65366e24e4",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFile, String ModelID, String TheRes){
        //https://{endpoint}/formrecognizer/v2.0/layout/analyze
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";
        String APIVersion = "v2.0";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        StringValue d = command.action("Default",TheFile,"MODEL",ModelID,APIVersion);

        System.out.println("URL:"+d.toString());

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
