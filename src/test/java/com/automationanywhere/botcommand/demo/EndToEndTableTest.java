package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class EndToEndTableTest {

    UploadDocument command = new UploadDocument();
    GetDocumentInfoAsJson command1 = new GetDocumentInfoAsJson();
    JSONTableOutputToCSV command2 = new JSONTableOutputToCSV();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

              //  {"C:\\iqbot\\Jaguar\\Input\\180305 - CHCM 2 SPCA Fully Signed - CW33246.pdf",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFile, String TheRes){
        //https://{endpoint}/formrecognizer/v2.0/layout/analyze
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";
        String APIVersion = "v2.0";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);


        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);
        command1.setSessions(mso);
        command2.setSessions(mso);

        StringValue d = command.action("Default",TheFile,"TABLE","",APIVersion);

        System.out.println("Result URL:"+d.toString());

        StringValue RawJSONOutput = command1.action("Default",d.toString(),true,90);

        ListValue lv = command2.action("Default",RawJSONOutput.toString());

        List<Value> allValues = lv.get();

        for(int k=0;k<allValues.size();k++) {
            TableValue tv = (TableValue) allValues.get(k);
            Table table = tv.get();
            List<Row> AllRows = table.getRows();

            for(int i=0;i<AllRows.size();i++){

                Row dv = AllRows.get(i);
                String aRowOfStrings = "";
                for(int j = 0;j < dv.getValues().size();j++){
                    Value myVal = dv.getValues().get(j);
                    if(aRowOfStrings.equals("")){
                        aRowOfStrings = myVal.get().toString();
                    }else{
                        aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                    }

                }
                System.out.println("DEBUG ["+k+"] Row "+i+": "+aRowOfStrings);

            }

        }

    }
}
