package com.automationanywhere.botcommand.demo;

import Utils.ApiConstants;
import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class EndToEndTableTest5 {


    UploadDocument command = new UploadDocument();
    GetDocumentInfoAsJson command1 = new GetDocumentInfoAsJson();
    JSONOutputToCSV command2 = new JSONOutputToCSV();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

                {"C:\\iqbot\\MSForms_Ita\\",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFolder, String TheRes){
        //https://{endpoint}/formrecognizer/v2.0/layout/analyze
        String URL = "https://euseformsrecogniser.cognitiveservices.azure.com/formrecognizer";
        String AuthToken ="72173c07c0bd49d99d557bb6c3e462b8";
        String MSModel = "bd159084-e082-4331-8aca-3494b5534f51";
        String APIVersion = "v2.0";

        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);
        command1.setSessions(mso);
        command2.setSessions(mso);

        File dir = new File(TheFolder);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                System.out.println("Uploading: "+child.getAbsolutePath());
                StringValue d = command.action("Default",child.getAbsolutePath(),"ID",MSModel,APIVersion);

                System.out.println("Result URL:"+d.toString());
                StringValue RawJSONOutput = command1.action("Default",d.toString(),true,90);

                TableValue lv = command2.action("Default",RawJSONOutput.toString());
                Table table = lv.get();
                List<Row> AllRows = table.getRows();

                for(int i=0;i<AllRows.size();i++){

                    Row dv = AllRows.get(i);
                    String aRowOfStrings = "";
                    for(int j = 0;j < dv.getValues().size();j++){
                        Value myVal = dv.getValues().get(j);
                        if(aRowOfStrings.equals("")){
                            aRowOfStrings = myVal.get().toString();
                        }else{
                            aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                        }

                    }
                    System.out.println("DEBUG Row "+i+": "+aRowOfStrings);

                }


            }
        } else {
            // Handle the case where dir is not really a directory.
            // Checking dir.isDirectory() above would not be sufficient
            // to avoid race conditions with another process that deletes
            // directories.
        }



    }
}
