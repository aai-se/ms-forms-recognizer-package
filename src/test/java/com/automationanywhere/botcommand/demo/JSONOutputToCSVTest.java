package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class JSONOutputToCSVTest {

    JSONOutputToCSV command = new JSONOutputToCSV();
    GetDocumentInfoAsJson command0 = new GetDocumentInfoAsJson();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

              //  {"https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/b5854174-6064-49bf-8258-da65366e24e4/analyzeresults/8fc2183f-410a-4b00-b2ee-8bfa704396c5",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheURL, String TheRes){
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer/v2.0/custom/models/";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";

        String TheFile = "C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Desktop\\Deloitte\\Form 1120_SCH L_1-2018-1.pdf";
        String ModelID = "94448316-7d88-4e79-9e66-6d349743ca7d";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);

        command.setSessions(mso);
        command0.setSessions(mso);

        StringValue val = command0.action("Default",TheURL,true,64);
        String JSONOutput = val.toString();
        //System.out.println("Callback URL:"+TheCallBackURL);
        TableValue d = command.action("Default",JSONOutput);
        Table table = d.get();
        List<Row> AllRows = table.getRows();

        for(int i=0;i<AllRows.size();i++){

            Row dv = AllRows.get(i);
            String aRowOfStrings = "";
            for(int j = 0;j < dv.getValues().size();j++){
                Value myVal = dv.getValues().get(j);
                if(aRowOfStrings.equals("")){
                    aRowOfStrings = myVal.get().toString();
                }else{
                    aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                }

            }
            System.out.println("DEBUG Row "+i+": "+aRowOfStrings);

        }

    }
}
