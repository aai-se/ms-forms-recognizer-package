package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class EndToEndModelTest {

    UploadDocument command = new UploadDocument();
    GetDocumentInfoAsJson command1 = new GetDocumentInfoAsJson();
    JSONOutputToCSV command2 = new JSONOutputToCSV();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{

               // {"C:\\iqbot\\MSForms\\88852 MCDONALD AUTOMOTIVE CLARE, INC. 0920 (1).pdf","b5854174-6064-49bf-8258-da65366e24e4",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFile, String ModelID, String TheRes){
        //https://{endpoint}/formrecognizer/v2.0/layout/analyze
        String URL = "https://iqbot.cognitiveservices.azure.com/formrecognizer";
        String AuthToken = "f856e71f033147cd82c109115ef6d51a";
        String APIVersion = "v2.0";
        BackendServer myBackendServ = new BackendServer(URL,AuthToken);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);
        command1.setSessions(mso);
        command2.setSessions(mso);

        StringValue d = command.action("Default",TheFile,"MODEL",ModelID,APIVersion);

        System.out.println("Result URL:"+d.toString());

        StringValue RawJSONOutput = command1.action("Default",d.toString(),true,90);
        //System.out.println(RawJSONOutput.get());
        TableValue tb = command2.action("Default",RawJSONOutput.toString());

        Table table = tb.get();
        List<Row> AllRows = table.getRows();

        for(int i=0;i<AllRows.size();i++){

            Row dv = AllRows.get(i);
            String aRowOfStrings = "";
            for(int j = 0;j < dv.getValues().size();j++){
                Value myVal = dv.getValues().get(j);
                if(aRowOfStrings.equals("")){
                    aRowOfStrings = myVal.get().toString();
                }else{
                    aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                }

            }
            System.out.println("DEBUG Row "+i+": "+aRowOfStrings);

        }
    }
}
